# Material Design Sample #

### Installation ###

What you will need to get started:

* [Node.js](https://nodejs.org/en/)
* Run ``` $ npm install ```

### NPM Scripts ###

* Build ``` $ npm run build ```
* Watch ``` $ npm run watch ```