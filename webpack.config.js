const path = require("path");
const webpack = require("webpack");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Directory Variables
const srcRootDir = path.join(__dirname, "src"); // same as "./src"
const distDir = path.join(__dirname, "dist"); // same as "./dist"



module.exports = {
    //devtool: 'inline-source-map', // Adds sourcemaps to the bundled JS file
    devtool: 'source-map', // Creates external sourcemap files

    context: srcRootDir, // Set the root for your entry files

    entry: { // Select the entry file(s)
        vendors: "./vendors.js", // same as "./src/vendors.js" because it is prefixed with the "context" path
        app: "./app.js", // same as "./src/app.js" because it is prefixed with the "context" path
    },

    output: {
        path: distDir,
        filename: "[name].bundle.js"
    },

    module: {
        rules: [{
                test: /\.(js|jsx)$/,
                include: /src/,
                use: "babel-loader"
            },
            {
                test: /\.(jp?g|png|gif|tiff|bmp|svg)$/,
                include: /images/,
                use: "file-loader?name=[path][name].[ext]"
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                include: /fonts/,
                use: "file-loader?name=fonts/[name].[ext]"
            },
            {
                test: /\.(scss|css)$/,
                include: /src/,
                // External CSS with sourcemaps
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        "css-loader?sourceMap",
                        "postcss-loader?sourceMap",
                        "sass-loader?sourceMap"
                    ]
                })
                // Bundled CSS with js (sourcemaps not supported when bundled with js)
                // use: [
                //     "style-loader",
                //     "css-loader",
                //     "sass-loader"
                // ]
            }
        ],
    },

    plugins: [
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: {
                baseDir: [distDir]
            }
        }),
        new CleanWebpackPlugin(['dist'], {
            verbose: true,
            dry: false
        }),
        new ExtractTextPlugin({
            filename: "[name].bundle.css",
            allChunks: true
        }),

        // HMTL Pages
        new HtmlWebpackPlugin({
            title: "Material Design Sample",
            template: srcRootDir + "/templates/index.html", // Load a custom template
        })
    ]

};